﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ElGamal
{
    class Program
    {
        static List<int> SimpleNum = new List<int>()
        { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97 };
        static string Symbols = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";

        static void Main(string[] args)
        {
            bool isOk = false;

            int p = 0;

            while (!isOk)
            {
                Console.Write("Введите натуральное число p:\np = ");
                p = Int32.Parse(Console.ReadLine());

                isOk = SimpleNum.Contains(p);

                if (!isOk)
                {
                    Console.WriteLine("Это не натуральное число");
                    Console.ReadKey();
                    Console.Clear();
                }
            }

            int g = GetPRoot(p);
            
            Random random = new Random();

            int x = random.Next(1, p - 1);
            int y = (int)(BigInteger.Pow(g, x) % p);

            Console.WriteLine($"\np = {p}");
            Console.WriteLine($"g = {g}");
            Console.WriteLine($"x = {x}");
            Console.WriteLine($"y = {y}");

            Console.WriteLine();
            Console.Write("Введите сообщение: ");
            string message = Console.ReadLine().ToUpper();
            Console.WriteLine();

            var encr = EncryptMessage(message, p, g, y);

            Console.WriteLine();

            Console.WriteLine("Зашифрованное сообщение:");
            foreach (var m in encr)
                Console.Write($"({m.Item1}, {m.Item2}) ");

            Console.WriteLine("\n");

            Console.WriteLine($"\nРасшифрованное сообщение: {DecryptMessage(encr, p, x)}");

            Console.Read();
        }

        //static int[] k = new int[] { 5, 36, 18, 7, 10, 22 };
            
        static List<(int, int)> EncryptMessage(string message, int p, int g, int y)
        {
            List<(int, int)> res = new List<(int, int)>();

            int i = 0;

            Random random = new Random();

            foreach (var s in message)
            {
                int k = random.Next(1, p - 1);
                int T = Symbols.IndexOf(s);
                int a = (int)(BigInteger.Pow(g, k) % p);
                int b = (int)((BigInteger.Pow(y, k) * T) % p);

                Console.WriteLine($"[{i+1}]: a = {a}, b = {b}, T = {T}");

                res.Add((a, b));
                i++;
            }

            return res;
        }

        static string DecryptMessage(List<(int, int)> encryptedMessage, int p, int x)
        {
            string res = string.Empty;

            Debug.WriteLine(p + " " + x);

            int i = 0;

            foreach (var m in encryptedMessage)
            {
                int a = m.Item1;
                int b = m.Item2;

                int T = (int)((b * BigInteger.Pow(a, p - 1 - x)) % p);

                Console.WriteLine($"[{i + 1}]: a = {a}, b = {b}, T = {T}");
                res += Symbols[T];

                i++;
            }

            return res;
        }

        static int Power(int x, int n)
        {
            if (n == 0)
                return 1;

            if (n % 2 == 0)
            {
                var p = Power(x, n / 2);
                return p * p;
            }
            else
                return x * Power(x, n - 1);
        }

        public static int GetPRoot(int p)
        {
            for (int i = 0; i < p; i++)
                if (IsPRoot(p, i))
                    return i;
            return 0;
        }

        public static bool IsPRoot(int p, int a)
        {
            if (a == 0 || a == 1)
                return false;
            int last = 1;

            List<int> set = new List<int>();
            for (int i = 0; i < p - 1; i++)
            {
                last = (last * a) % p;
                if (set.Contains(last))
                    return false;
                set.Add(last);
            }
            return true;
        }
    }
}
